package com.restaurant.data.util

object EndpointUrl {

    const val RESTAURANTS = "api/v2.1/search"
}

object QueryParam {
    const val START = "start"
    const val LAT = "lat"
    const val LON = "lon"
    const val RADIUS = "radius"
}

object Header {
    const val USER_KEY = "user-key"
}