package com.restaurant.data.util

import android.webkit.URLUtil
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder

class RequestManager(private val baseUrl: String, private val apiKey: String) {

    private val CONNECTION_TIME_OUT = 10000
    private val READ_TIME_OUT = 20000

    fun sendGetRequest(
        queryParams: MutableMap<String, Any>?,
        endPointUrl: String
    ): Observable<String> {
        return Observable.just(baseUrl)
            .observeOn(Schedulers.io())
            .map { baseUrl ->
                val completeUrl = StringBuilder(baseUrl)
                    .append(endPointUrl)
                if (queryParams != null && queryParams.isNotEmpty()) {
                    queryParams.keys.forEachIndexed { index, key ->
                        completeUrl.append("?q=")
                        if (index == 0) {
                            completeUrl.append("?q=")
                        }
                        completeUrl.append(URLEncoder.encode(key, "UTF-8"))
                        completeUrl.append(URLEncoder.encode(queryParams[key].toString(), "UTF-8"))
                    }
                }
                completeUrl.toString()
            }
            .flatMap { url ->
                if (!URLUtil.isHttpsUrl(url) && !URLUtil.isHttpUrl(url)) {
                    Observable.error(Exception("Invalid url"))
                } else {
                    // Create url Object and connect
                    val urlObject = URL(url)
                    val httpURLConnection = urlObject.openConnection() as HttpURLConnection
                    httpURLConnection.requestMethod = "GET"
                    httpURLConnection.setRequestProperty(Header.USER_KEY, apiKey)
                    httpURLConnection.connectTimeout = CONNECTION_TIME_OUT
                    httpURLConnection.readTimeout = READ_TIME_OUT
                    // Download input stream and read
                    val inputStream = httpURLConnection.inputStream
                    val inputStreamReader = InputStreamReader(inputStream)
                    val bufferedReader = BufferedReader(inputStreamReader)
                    val response = StringBuilder()
                    var lineData = bufferedReader.readLine()
                    while (lineData != null) {
                        response.append(lineData)
                        lineData = bufferedReader.readLine()
                    }
                    bufferedReader.close()
                    inputStreamReader.close()
                    inputStream.close()
                    httpURLConnection.disconnect()
                    Observable.just(response.toString())
                }
            }
    }
}