package com.restaurant.data.model

data class RestaurantsResponse (
    val resultsFound: Long? = null,
    val resultsStart: Long? = null,
    val resultsShown: Long? = null,
    val restaurants: List<RestaurantData>? = null
)

data class RestaurantData (
    val restaurant: Restaurant? = null
)

data class Restaurant (
    val id: String? = null,
    val name: String? = null,
    val thumb: String? = null,
    val url: String? = null,
    val location: Location? = null,
)

data class Location (
    val address: String? = null,
    val locality: String? = null,
    val city: String? = null,
    val cityID: Long? = null,
    val latitude: String? = null,
    val longitude: String? = null,
    val zipcode: String? = null,
    val countryID: Long? = null,
    val localityVerbose: String? = null
)