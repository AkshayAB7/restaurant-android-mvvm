package com.restaurant.data.remote

import com.restaurant.data.model.Restaurant
import io.reactivex.Observable

interface RestaurantRemote {

    fun getNewsFromNetwork(
        pageOffset: Int,
        latitude: Double,
        longitude: Double,
        radius: Int
    ): Observable<List<Restaurant>>
}