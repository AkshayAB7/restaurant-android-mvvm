package com.restaurant.data.remote

import com.google.gson.Gson
import com.restaurant.data.model.Restaurant
import com.restaurant.data.model.RestaurantsResponse
import com.restaurant.data.util.EndpointUrl
import com.restaurant.data.util.QueryParam
import com.restaurant.data.util.RequestManager
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class RestaurantRemoteImpl(
    private val requestManager: RequestManager,
    private val responseParser: Gson
) : RestaurantRemote {

    override fun getNewsFromNetwork(
        pageOffset: Int,
        latitude: Double,
        longitude: Double,
        radius: Int
    ): Observable<List<Restaurant>> {
        val queries = mutableMapOf<String, Any>(
            QueryParam.START to pageOffset,
            QueryParam.LAT to latitude,
            QueryParam.LON to longitude,
            QueryParam.RADIUS to radius
        )
        return requestManager.sendGetRequest(queries, EndpointUrl.RESTAURANTS)
            .observeOn(Schedulers.io())
            .map { response ->
                responseParser.fromJson(
                    response,
                    RestaurantsResponse::class.java
                ).restaurants?.mapNotNull { it.restaurant }
            }
    }
}