package com.restaurant.data

import com.restaurant.data.exception.NoNetworkException
import com.restaurant.data.model.Restaurant
import com.restaurant.data.remote.RestaurantRemote
import com.restaurant.data.repository.RestaurantRepository
import io.reactivex.Observable
import io.reactivex.subjects.Subject

class RestaurantRepositoryImpl(
    private val restaurantRemote: RestaurantRemote,
    private val networkAvailabilityObservable: Subject<Boolean>
) : RestaurantRepository {

    override fun getRestaurants(
        pageOffset: Int,
        latitude: Double,
        longitude: Double,
        radius: Int
    ): Observable<List<Restaurant>> {
        return networkAvailabilityObservable
            .flatMap { networkAvailable ->
                Observable.just(
                    networkAvailable
                )
            }.flatMap { networkAvailable ->
                // If no network and cache data present, return cached data
                if (!networkAvailable) {
                    Observable.error(NoNetworkException())
                } else {
                    // Get data from remote, delete old cache, save new data to cache and return
                    restaurantRemote.getNewsFromNetwork(pageOffset, latitude, longitude, radius)
                }
            }
    }
}