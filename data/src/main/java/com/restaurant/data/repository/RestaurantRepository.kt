package com.restaurant.data.repository

import com.restaurant.data.model.Restaurant
import io.reactivex.Observable

interface RestaurantRepository {

    fun getRestaurants(
        pageOffset: Int,
        latitude: Double,
        longitude: Double,
        radius: Int
    ): Observable<List<Restaurant>>
}