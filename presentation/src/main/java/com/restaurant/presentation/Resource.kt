package com.restaurant.presentation

sealed class Resource<T> {
    // Loading state
    class Loading<T> : Resource<T>()
    // Success state
    data class Success<T>(val data: T) : Resource<T>()
    // Failure state
    data class Failure<T>(val throwable: Throwable) : Resource<T>()
}