package com.restaurant.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.restaurant.data.model.Restaurant
import com.restaurant.data.repository.RestaurantRepository
import com.restaurant.presentation.Resource
import com.restaurant.presentation.util.ObservableSchedulerTransformer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

class RestaurantViewModel(
    private val restaurantRepository: RestaurantRepository
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    fun getRestaurants(
        pageOffset: Int,
        latitude: Double,
        longitude: Double,
        radius: Int
    ): MutableLiveData<Resource<List<Restaurant>>> {
        val restaurantObservable = MutableLiveData<Resource<List<Restaurant>>>()
        Log.d("Page", "$pageOffset")
        restaurantObservable.value = Resource.Loading()
        val disposable =
            restaurantRepository.getRestaurants(pageOffset, latitude, longitude, radius)
                .compose(ObservableSchedulerTransformer()).subscribe({ restaurants ->
                    restaurantObservable.value = Resource.Success(restaurants)
                }, { error ->
                    restaurantObservable.value = Resource.Failure(error)
                })
        disposable.addTo(compositeDisposable)
        return restaurantObservable
    }


    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }
}