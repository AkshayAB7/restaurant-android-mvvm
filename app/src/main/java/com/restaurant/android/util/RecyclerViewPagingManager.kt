package com.restaurant.android.util

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE

abstract class RecyclerViewPagingManager(recyclerView: RecyclerView) :
    RecyclerView.OnScrollListener() {

    private var isPaging = false
    private val layoutManager: RecyclerView.LayoutManager? = recyclerView.layoutManager
    var previousLoadedSize: Int = 0
        set(value) {
            field += value
        }
    abstract val isLastPage: Boolean

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dy > 0) {
            val totalItemCount = layoutManager?.itemCount ?: 0
            var lastVisibleItemPosition = 0
            if (layoutManager is LinearLayoutManager) {
                lastVisibleItemPosition = layoutManager.findLastCompletelyVisibleItemPosition()
            }
            if (isLastPage) return
            if (lastVisibleItemPosition == totalItemCount - 1) {
                if (!isPaging) {
                    isPaging = true
                    loadMore(previousLoadedSize)
                }
            } else {
                isPaging = false
            }
        }
    }

    fun reset() {
        previousLoadedSize = 0
    }

    abstract fun loadMore(offset: Int)
}