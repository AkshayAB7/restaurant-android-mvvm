package com.restaurant.android.util

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject

class NetworkConnectivityManager(application: Application) : NetworkConnectivitySource {

    private var networkConnectivitySubject: BehaviorSubject<Boolean> = BehaviorSubject.create()

    init {
        networkConnectivitySubject.onNext(false)
        registerForConnectivity(application)
    }

    private fun registerForConnectivity(application: Application) {
        val connectivityManager =
            application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkRequest = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .build()
        connectivityManager.registerNetworkCallback(networkRequest, object :
            ConnectivityManager.NetworkCallback() {
            override fun onLost(network: Network) {
                networkConnectivitySubject.onNext(false)
            }

            override fun onUnavailable() {
                networkConnectivitySubject.onNext(false)
            }

            override fun onLosing(network: Network, maxMsToLive: Int) {
                networkConnectivitySubject.onNext(false)
            }

            override fun onAvailable(network: Network) {
                networkConnectivitySubject.onNext(true)
            }
        })
    }

    override fun isNetworkConnectivityAvailable(): Subject<Boolean> {
        return networkConnectivitySubject
    }
}