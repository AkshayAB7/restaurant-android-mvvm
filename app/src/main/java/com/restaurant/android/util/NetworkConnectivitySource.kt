package com.restaurant.android.util

import io.reactivex.subjects.Subject

interface NetworkConnectivitySource {

    fun isNetworkConnectivityAvailable(): Subject<Boolean>
}