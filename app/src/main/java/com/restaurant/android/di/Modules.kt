package com.restaurant.android.di

import com.google.gson.Gson
import com.restaurant.android.BuildConfig
import com.restaurant.android.util.ImageLoader
import com.restaurant.android.util.NetworkConnectivityManager
import com.restaurant.android.util.NetworkConnectivitySource
import com.restaurant.data.util.RequestManager
import com.restaurant.data.RestaurantRepositoryImpl
import com.restaurant.data.remote.RestaurantRemote
import com.restaurant.data.remote.RestaurantRemoteImpl
import com.restaurant.data.repository.RestaurantRepository
import com.restaurant.presentation.viewmodel.RestaurantViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module


val allModules by lazy {
    listOf(
        appModule,
        viewModelModule,
        dataModule
    )
}

val appModule: Module = module {
    single<NetworkConnectivitySource> { NetworkConnectivityManager(androidApplication()) }
    single { ImageLoader.getInstance() }
}

val viewModelModule: Module = module {
    viewModel { RestaurantViewModel(get()) }
}

val dataModule: Module = module {
    single<RestaurantRepository> {
        RestaurantRepositoryImpl(
            restaurantRemote = get(),
            networkAvailabilityObservable = get<NetworkConnectivitySource>().isNetworkConnectivityAvailable()
        )
    }
    single<RestaurantRemote> { RestaurantRemoteImpl(requestManager, getGson()) }
}

// Network initialization
private val requestManager = createRequestManager(BuildConfig.ZOMATO_BASE_URL, BuildConfig.ZOMATO_API_KEY)

private fun createRequestManager(baseUrl: String, apiKey: String): RequestManager {
    return RequestManager(baseUrl, apiKey)
}

private fun getGson(): Gson {
    return Gson()
}