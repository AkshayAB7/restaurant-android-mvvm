package com.restaurant.android.ui.restaurant.list

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.restaurant.android.R
import com.restaurant.android.ui.base.BaseActivity
import com.restaurant.android.util.ImageLoader
import com.restaurant.android.util.RecyclerViewPagingManager
import com.restaurant.data.model.Restaurant
import com.restaurant.presentation.Resource
import com.restaurant.presentation.viewmodel.RestaurantViewModel
import kotlinx.android.synthetic.main.activity_restaurant.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class RestaurantsActivity : BaseActivity(), RestaurantsItemClickListener {

    private val LOCATION_REQUEST_CODE = 100
    private val restaurantViewModel: RestaurantViewModel by viewModel()
    private val imageLoader: ImageLoader by inject()
    private var restaurantsListAdapter: RestaurantsListAdapter? = null
    private lateinit var pageManager: RecyclerViewPagingManager
    private lateinit var responseObserver: Observer<Resource<List<Restaurant>>>
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private var radius: Int = 2000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant)
        initViews()
        checkPermissionOrRequest()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation()
                } else {
                    val showMessageRequest = ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                    if (showMessageRequest) {
                        showMessage(
                            getString(R.string.location_permission_error)
                        ) { checkPermissionOrRequest() }
                    } else {
                        showMessage(getString(R.string.permenent_deny))
                    }
                }
            }
        }
    }

    private fun checkPermissionOrRequest() {
        if (!isLocationPermissionGranted()) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_REQUEST_CODE
            )
        } else {
            getLocation()
        }
    }

    private fun isLocationPermissionGranted(): Boolean {
        val permission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        return permission == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    private fun getLocation() {
        val locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
        locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, object :
            LocationListener {
            override fun onLocationChanged(location: Location) {
                latitude = location.latitude
                longitude = location.longitude
                locationManager.removeUpdates(this)
                getRestaurants(
                    pageManager.previousLoadedSize,
                    latitude,
                    longitude,
                    radius,
                    responseObserver
                )
            }

            override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
            override fun onProviderEnabled(provider: String) {}
            override fun onProviderDisabled(provider: String) {}
        })
    }

    private fun initViews() {
        // Set toolbar
        setSupportActionBar(toolBar)
        // Init LayoutManager, DividerItemDecorator and adapter
        rvRestaurants.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        val dividerItemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(this, R.drawable.ic_divider)!!)
        rvRestaurants.addItemDecoration(dividerItemDecoration)
        restaurantsListAdapter = RestaurantsListAdapter(null, this, imageLoader)
        rvRestaurants.adapter = restaurantsListAdapter
        // Pager and observer init
        initPagerAndObserver()
        // Init pull to refresh
        srl_restaurants.setOnRefreshListener {
            srl_restaurants.isRefreshing = false
            if (isLocationPermissionGranted()) {
                pageManager.reset()
                restaurantsListAdapter?.clearRestaurants()
                getRestaurants(
                    pageManager.previousLoadedSize,
                    latitude,
                    longitude,
                    radius,
                    responseObserver
                )
            } else {
                checkPermissionOrRequest()
            }
        }
        rvRestaurants.addOnScrollListener(pageManager)
    }

    private fun initPagerAndObserver() {
        pageManager = object : RecyclerViewPagingManager(rvRestaurants) {
            override val isLastPage: Boolean
                get() = false

            override fun loadMore(offset: Int) {
                getRestaurants(
                    offset,
                    latitude,
                    longitude,
                    radius,
                    responseObserver
                )
            }
        }
        responseObserver = Observer<Resource<List<Restaurant>>> { resource ->
            when (resource) {
                is Resource.Loading -> {
                    pbLoading.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    val restaurants = resource.data
                    pageManager.previousLoadedSize = restaurants.size
                    restaurantsListAdapter?.updateRestaurantsList(restaurants)
                    pbLoading.visibility = View.GONE
                }
                is Resource.Failure -> {
                    pbLoading.visibility = View.GONE
                    showMessage(resource.throwable)
                }
            }
        }
    }

    private fun getRestaurants(
        previousLoadedSize: Int,
        latitude: Double,
        longitude: Double,
        radius: Int,
        responseObserver: Observer<Resource<List<Restaurant>>>
    ) {
        restaurantViewModel.getRestaurants(previousLoadedSize, latitude, longitude, radius)
            .observe(this, responseObserver)
    }

    override fun onNewsItemClicked(restaurant: Restaurant?) {
        Snackbar.make(rvRestaurants, restaurant?.name ?: "", Snackbar.LENGTH_SHORT).show()
    }
}