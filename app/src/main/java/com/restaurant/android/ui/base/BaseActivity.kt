package com.restaurant.android.ui.base

import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.restaurant.android.R
import com.restaurant.data.exception.NoNetworkException

abstract class BaseActivity : AppCompatActivity() {

    fun showMessage(message: String) {
        AlertDialog.Builder(this).setMessage(message).setCancelable(false).setPositiveButton(
            getString(R.string.ok)
        ) { dialog, _ -> dialog?.dismiss() }.show()
    }

    fun showMessage(throwable: Throwable) {
        val message =
            if (throwable is NoNetworkException) getString(R.string.no_network) else throwable.localizedMessage
        AlertDialog.Builder(this).setMessage(message).setCancelable(false).setPositiveButton(
            getString(R.string.ok)
        ) { dialog, _ -> dialog?.dismiss() }.show()
    }

    fun showMessage(message: String, action: () -> Unit) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                dialog.dismiss()
                action.invoke()
            }
            .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }
}