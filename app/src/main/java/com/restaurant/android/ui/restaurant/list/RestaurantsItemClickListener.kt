package com.restaurant.android.ui.restaurant.list

import com.restaurant.data.model.Restaurant


interface RestaurantsItemClickListener {

    fun onNewsItemClicked(restaurant: Restaurant?)
}