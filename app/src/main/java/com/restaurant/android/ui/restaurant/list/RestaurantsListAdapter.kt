package com.restaurant.android.ui.restaurant.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.android.R
import com.restaurant.android.databinding.ItemRestaurantBinding
import com.restaurant.android.util.ImageLoader
import com.restaurant.data.model.Restaurant

class RestaurantsListAdapter(
    articleList: List<Restaurant>?,
    private val clickListener: RestaurantsItemClickListener,
    private val imageLoader: ImageLoader
) : RecyclerView.Adapter<RestaurantsListAdapter.ViewHolder>() {

    private var restaurantsList = mutableListOf<Restaurant>()

    init {
        if (articleList != null) {
            this.restaurantsList.addAll(articleList)
        }
    }

    fun clearRestaurants() {
        this.restaurantsList.clear()
    }

    fun updateRestaurantsList(restaurantList: List<Restaurant>?) {
        if (restaurantList != null) {
            this.restaurantsList.addAll(restaurantList)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemRestaurantBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), clickListener, imageLoader
        )
    }

    override fun getItemCount(): Int = restaurantsList.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(restaurantsList[position])
    }

    override fun onViewRecycled(holder: ViewHolder) {
        holder.reset()
        super.onViewRecycled(holder)
    }

    class ViewHolder(
        private val itemBinding: ItemRestaurantBinding,
        private val clickListener: RestaurantsItemClickListener,
        private val imageLoader: ImageLoader
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        init {
            itemBinding.root.setOnClickListener {
                clickListener.onNewsItemClicked(restaurant)
            }
        }

        private var restaurant: Restaurant? = null

        fun bind(restaurant: Restaurant) {
            this.restaurant = restaurant
            itemBinding.tvNewsTitle.text = restaurant.name
            itemBinding.tvNewsDescription.text = restaurant.location?.address
            imageLoader.load(itemBinding.ivNews, restaurant.thumb?: "")
        }

        fun reset() {
            itemBinding.tvNewsTitle.text = ""
            itemBinding.tvNewsDescription.text = ""
            itemBinding.ivNews.setImageResource(R.drawable.ic_image_placeholder)
        }
    }
}