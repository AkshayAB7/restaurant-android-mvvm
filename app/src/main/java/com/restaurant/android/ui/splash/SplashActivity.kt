package com.restaurant.android.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.restaurant.android.R
import com.restaurant.android.ui.base.BaseActivity
import com.restaurant.android.ui.restaurant.list.RestaurantsActivity

class SplashActivity: BaseActivity() {

    private val handler = Handler(Looper.getMainLooper())
    private val runnable = {
        startActivity(Intent(this, RestaurantsActivity::class.java))
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        handler.postDelayed(runnable, 1500)
    }

    override fun onDestroy() {
        handler.removeCallbacks(runnable)
        super.onDestroy()
    }
}